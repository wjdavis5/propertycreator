﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;


namespace PropertyCreator
{
    
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
             
        }

        private void generatePropsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            ThreadPool.QueueUserWorkItem((new WaitCallback(doUpdate)));
            tType.Focus();                

        }
        private void doUpdate(object stateInfo)
        {
            Thread.CurrentThread.TrySetApartmentState(ApartmentState.STA);
            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            StringBuilder sb3 = new StringBuilder();
            string myType = tType.Text;
            string myName = tName.Text;
            ExecuteSecure(() => tType.Text = "");
            ExecuteSecure(() => tName.Text = "");
            sb.AppendLine("private " + myType + " _" + myName + "{get;set;}");
            sb2.AppendLine("public " + myType + " " + myName + "{get{return _" + myName + ";}}");
            sb3.AppendLine("// _" + myName + "=;");
            ExecuteSecure(() => textBox2.Text += sb.ToString());
            ExecuteSecure(() => textBox1.Text += sb2.ToString());
            ExecuteSecure(() => textBox3.Text += sb3.ToString());
            ExecuteSecure(() => textBox1.SelectionStart = textBox1.TextLength);
            ExecuteSecure(() => textBox1.ScrollToCaret());
            ExecuteSecure(() => textBox2.SelectionStart = textBox2.TextLength);
            ExecuteSecure(() => textBox2.ScrollToCaret());
            ExecuteSecure(() => textBox3.SelectionStart = textBox3.TextLength);
            ExecuteSecure(() => textBox3.ScrollToCaret());
            StringBuilder sb4 = new StringBuilder();
            sb4.Append(textBox1.Text);
            sb4.Append("\r\n");
            sb4.Append(textBox2.Text);
            sb4.Append("\r\n");
            sb4.Append(textBox3.Text);
            sb4.Append("\r\n");
            ExecuteSecure(() => Clipboard.SetText(sb4.ToString()));
        }

        private void ExecuteSecure(Action a)
        {
            if (InvokeRequired)
            {
                BeginInvoke(a);
            }
            else
            {
                a();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tType.Focus();
        }
    }
}
